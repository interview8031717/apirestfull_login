import { Controller, Post, Body } from '@nestjs/common';
import {AutenticacionService} from './autenticacion.service';

@Controller('autenticacion')
export class AutenticacionController {
    constructor(private readonly authService: AutenticacionService) {}

    @Post('registrar')
    async registrar(@Body() body: { usaurio: string; password: string }) {
      let valor = true;
      const res = await this.authService.registrar(body.usaurio, body.password);
      if (!res) {
        valor = false;
        return { message: 'success', valor: res };
      }
      return { message: 'success', valor,  };
    }
  
    @Post('sesion')
    async sesion(@Body() body: { usaurio: string; password: string }) {
      const res = await this.authService.sesion(body.usaurio, body.password);
      if (res == false) {
        return { message: 'success', valor: res };
      }
      return { message: 'Usuario logueado exitosamente', valor:res };
    }    
}
