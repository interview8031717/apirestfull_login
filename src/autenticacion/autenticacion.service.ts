import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

interface User {
    usuario: string;
    password: string;
}

@Injectable()
export class AutenticacionService {
    private readonly users: User[] = [];

    async registrar(usuario: string, password:string)
    {
        const hashedPassword = await bcrypt.hash(password, 10);
        const user = { usuario, password: hashedPassword };
        const res = this.users.push(user);
        if (!res) {
            return false;
        }
        
        return user;       
    }

    async sesion(usuario: string, password: string) {
        const user = this.users.find((u) => u.usuario === usuario);
        if (!user) {
          return false;
        }
    
        const isValidPassword = await bcrypt.compare(password, user.password);
        if (!isValidPassword) {
          return false;
        }
    
        return 'JWT token';
      }    

}
