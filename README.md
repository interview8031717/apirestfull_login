<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a>
<a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
<a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg"/></a>
    <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).


## PASOS PARA INICIAR UNA APLICACIÓN DESDE CERO

## INSTALAR NESTJS
Podemos seguir la documentación oficial en:
https://docs.nestjs.com/

## Instalación
     npm i -g @nestjs/cli
## Crear nuevo proyecto
     nest new project-name

## Si tenemos errores al inicializar por primera vez podemos correr nuevamente
   npm install

## Levantar la aplicacion
   npm run start

## Podemos crear un controlador 

  nest g controller name

## tambien con sus recursos
  nest g resource name

## creamos su modulo autenticacion
   nest g module autenticacion

## creamos el controlador autenticacion
  nest g controller autenticacion

## creamos su servicio
   nest g service autenticacion

## testear API
## JSON de prueba
{
    "usuario":"JUAN",
    "password":"12345678"
}

## end-point para registrar usuario
http://localhost:3000/autenticacion/registrar


## end-point para iniciar sesion
http://localhost:3000/autenticacion/sesion


## experiencia en el reto
Utilice modulos, controladores y servicios, para realizar el desarrollo requerido,
queda campo para explorar, para entender y por ende para mejorar, ya que es un 
framework nuevo para mi. 

Hoy en día podemos utilizar todas las herramientas que tenemos en nuestro entorno
para crear una solución, podemos apoyarnos con personas con conocimientos avanzados,
lo cual nos permite adaptarnos muchos más rapido a entornos de desarrollo de rapido 
crecimiento.

La Inteligencia Artificial, es un gran aleado, que nos permite entendender muchos más 
rapido un nuevo lenguaje de programación. 

Lo cual nos permite adaptarnos más rapido a los cambios, solamente debemos de estar 
dispuestos a aprender y a leer la documentación, para sacar el mayor provecho a cada 
herramienta.



